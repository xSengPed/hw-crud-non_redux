import React, { useEffect, useState } from 'react'
import axios from 'axios'
import StudentCard from './StudentCard'
import "./StudentCard.css";
const StudentList = (props) => {
    
    if (!props.students || !props.students.length)
        return (<h2 id="notify">No Student</h2>)

    return (
        <div className="student-list-con">
            {
                props.students.map((student, index) => (
                    <div key={index} style={{ margin: 5 }}>
                        <StudentCard {...student} updateStudent={() => props.updateStudent(student.id)} deleteStudent={() => props.deleteStudent(student.id)}/>
                    </div>
                ))
            }
        </div>

    )
} 

export default StudentList