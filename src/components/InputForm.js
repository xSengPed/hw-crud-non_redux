import React, { useEffect, useState } from "react";
import { Input, Row, Col, Button, InputNumber } from "antd";
import "./input.css";
import axios from "axios";

const InputForm = props => {
  
  return (

    <div className="container">
      <div className="frame-border card">
        <p id="header">
          <h2>Create Student</h2>
        </p>
        <p>
          <Input
            
            onChange={(e)=>setName(e.target.value)}
            placeholder="Name"
          />
        </p>
        <p>
          <Input
            
            onChange={(e)=>setSurname(e.target.value)}
            placeholder="Surname"
          />
        </p>
        <p>
          <Input
            
            onChange={(e)=>setMajor(e.target.value)}
            placeholder="Major"
          />
        </p>
        <p>
          GPA :{" "}
          <Input
            onChange={(e)=>setGpa(e.target.value)}
            placeholder="Grade Point Average"
          />
        </p>
        <div id="btn">
          <Button 
          
          onClick={()=>addStudent(name,surname,major,gpa)}
          type="primary">
            Create
          </Button>{" "}
        </div>
      </div>
      <div></div>
    </div>
  );
};
export default InputForm;
