import React , {useState}from "react";
import axios from "axios";

import "./StudentCard.css";
import { Button, Row, Col } from "antd";

const StudentCard = props => {
  
  return (
    <div>
      <div className="card-container">
        <p>
          <h2>{props.id}</h2>
        </p>
        <p>
          <h2>{props.name + " " + props.surname}</h2>
        </p>
        <p>{"Major : " + props.major}</p>
        <p>{"GPA : " + props.gpa}</p>
        <Row>
          <Col>
            <Button type="danger" onClick={props.deleteStudent}>
              Delete
              
            </Button>{" "}
            <Button type="primary" onClick={props.updateStudent}>
              Update
            </Button>
          </Col>
        </Row>
      </div>
    </div>
  );
};
export default StudentCard;
