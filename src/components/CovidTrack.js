import React, { useEffect } from 'react'
import axios from 'axios'

const CovidTrack = () => {
    const getCovidData = async () => {
        let result = await axios.get(`https://covid19-cdn.workpointnews.com/api/constants.json`)
        console.log(result.data)
    }
    
    useEffect(()=>{
        getCovidData()
    },[])
    return (
        <div>
            {result.data}
        </div>
    )
}

export default CovidTrack